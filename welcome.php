<?php
// Variables riceved from the form
$userFirstName = test_input(ucfirst($_POST['first_name']));
$userLastName = test_input(ucfirst($_POST['last_name']));
$userPassword = $_POST['password'];
$userDateOfBirthday = $_POST['dob'];
$userEmail = test_input($_POST['email']);




// First and last name check
if (ctype_alpha($userFirstName) && strlen($userFirstName) >= 3) {
    if ($userFirstName !== $userLastName) {
    } else {
        $warningMessage = 'The first name must be different from the last name';
        header("Location: error.php?wm=$warningMessage"); //Add redirect
    }

} else {
    $warningMessage = 'Enter plase the correct first name';
    header("Location: error.php?wm=$warningMessage"); //Add redirect
}
if (ctype_alpha($userLastName) && strlen($userLastName) >= 3) {
    if ($userLastName !== $userFirstName) {
    } else {
        $warningMessage = 'The last name must be different from the firs name';
        header("Location: error.php?wm=$warningMessage"); //Add redirect
    }

} else {
    $warningMessage = 'Enter plase the correct last name'; //Add redirect
    header("Location: error.php?wm=$warningMessage");
}

// Validate email
if (filter_var($userEmail, FILTER_VALIDATE_EMAIL)){
    $userEmailValid = "your email address <b>$userEmail</b> accepted";
} else {
    $userEmailInvalid = "This is not a valid email address";
    header("Location: error.php?wm=$userEmailInvalid"); //Add redirect
}

// Password verification
if(!empty($userPassword)) {
    if (strlen($userPassword) <= '6') {
        $passwordErr = "Your password must contain at least 6 characters!";
        header("Location: error.php?wm=$passwordErr"); //Add redirect
    }
    elseif(!preg_match("#[0-9]+#",$userPassword)) {
        $passwordErr = "Your password must contain at least 1 number!";
        header("Location: error.php?wm=$passwordErr"); //Add redirect
    }
    elseif(!preg_match("#[A-Z]+#",$userPassword)) {
        $passwordErr = "Your password must contain at least 1 capital letter!";
        header("Location: error.php?wm=$passwordErr"); //Add redirect
    }
    elseif(!preg_match("#[a-z]+#",$userPassword)) {
        $passwordErr = "Your password must contain at least 1 lowercase letter!";
        header("Location: error.php?wm=$passwordErr"); //Add redirect
    }
    elseif(!preg_match("#\W+#", $userPassword ) ) {
        $passwordErr = " Your password must include at least one symbol!";
        header("Location: error.php?wm=$passwordErr"); //Add redirect
    } else {
        $passwordErr = "Strong Your Password!";
    }
}
// Function of checking the variables obtained by the post method
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Test site - home work</title>

    <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i,600|Montserrat:200,300,400" rel="stylesheet">

    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="assets/fonts/ionicons/css/ionicons.min.css">

    <link rel="stylesheet" href="assets/fonts/fontawesome/css/font-awesome.min.css">


    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/slick-theme.css">

    <link rel="stylesheet" href="assets/css/helpers.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/landing-2.css">
</head>
<body data-spy="scroll" data-target="#pb-navbar" data-offset="200">

<nav class="navbar navbar-expand-lg navbar-dark pb_navbar pb_scrolled-light" id="pb-navbar">
    <div class="container">
        <a class="navbar-brand" href="index.php">My first site</a>
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#probootstrap-navbar" aria-controls="probootstrap-navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span><i class="ion-navicon"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="probootstrap-navbar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="index.php">Registration form</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->




<section class="pb_cover_v3 overflow-hidden cover-bg-indigo cover-bg-opacity text-left pb_gradient_v1 pb_slant-light" id="section-home">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-6">
                <h2 class="heading mb-3">WELCOME</h2>
                <h3 class="heading mb-3">
                    <body>
                    <?= "<b>$userFirstName $userLastName</b><br>"; ?>
                    </body>
                </h3>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-5 relative align-self-center">

                <form action=""  class="bg-white rounded pb_form_v1">
                    <h2 class="mb-4 mt-0 text-center">Congratulations!</h2>
                    <h4 class="mb-4 mt-0 text-center">
                        <?php
                        echo "You have successfully registered on our site and $userEmailValid ";
                        ?>
                    </h4>
                </form>

            </div>
        </div>
    </div>
</section>
<!-- END section -->


<footer class="pb_footer bg-light" role="contentinfo">
    <div class="container">
        <div class="row text-center">
            <div class="col">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="#" class="p-2"><i class="fa fa-facebook"></i></a></li>
                    <li class="list-inline-item"><a href="#" class="p-2"><i class="fa fa-twitter"></i></a></li>
                    <li class="list-inline-item"><a href="#" class="p-2"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <p class="pb_font-14">&copy; 2019. All Rights Reserved Puyo Mykola :-). <br> </p>
            </div>
        </div>
    </div>
</footer>

<!-- loader -->
<div id="pb_loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#1d82ff"/></svg></div>



<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/jquery.mb.YTPlayer.min.js"></script>

<script src="assets/js/jquery.waypoints.min.js"></script>
<script src="assets/js/jquery.easing.1.3.js"></script>

<script src="assets/js/main.js"></script>

</body>
</html>



